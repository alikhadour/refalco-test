<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function __construct()
    {
        $this->middleware('can:add product', ['only' => 'create','store']);
        $this->middleware('can:edit product', ['only' => 'edit','update']);
        $this->middleware('can:delete product', ['only' => 'destroy']);
        $this->middleware('can:view product', ['index' => 'show']);

    }
    public function index()
    {
        $products = Product::all();
        return view('products.index', compact('products')) ;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $sizes = Size::all();
        return view('products.create', compact('sizes')) ;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|file|mimes:jpeg,jpg,png',
            'name' => 'required',
            'price' => 'required:number',
            'size' => 'required'
        ]);

        $product =  new Product();
        $product->image = $this->save_image($request->file('image'));
        $product->name = $request->name;
        $product->price = $request->price  ;
        $product->size = $request->size ;
        $product->user_id = auth()->user()->id ;
        $product->save() ;

        return redirect()->back()
        ->with('success', 'Product Created Successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        $sizes = Size::all();
        $product = Product::find($product->id);
        return view('products.show', compact('product','sizes')) ;

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        $sizes = Size::all();
        $product = Product::find($product->id);
        return view('products.edit', compact('product', 'sizes')) ;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'image' => 'required|file|mimes:jpeg,jpg,png',
            'name' => 'required',
            'price' => 'required:number',
            'size' => 'required'
        ]);

        $product =  Product::find($product->id);
        $product->image = $this->save_image($request->file('image'));
        $product->name = $request->name;
        $product->price = $request->price  ;
        $product->size = $request->size ;
        $product->user_id = auth()->user()->id ;
        $product->save() ;

        return redirect()->back()
        ->with('success', 'Product Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        Product::find($product->id)->delete();
        return redirect()-back()
            ->with('success', "Deleted Successfully");
    }

    public function add_size(Request $request)
    {
        $size = new Size();
        $size->name = $request->name;
        $size->save();
        return redirect()->back()
        ->with('success', 'Added Successfully');
    }

    protected function save_image($image)
    {
        $fileName   = time() . '.' . $image->getClientOriginalExtension();

        // create an image manager instance with favored driver
        $manager = new ImageManager(['driver' => 'imagick']);
        $img = $manager->make($image->getRealPath());
        // $img->resize(120, 120, function ($constraint) {
        //     $constraint->aspectRatio();
        // });
        $img->stream(); // <-- Key point
        //dd();
        Storage::disk('public')->put('images/products' . '/' . $fileName, $img, 'public');
        return $fileName;
    }

}
