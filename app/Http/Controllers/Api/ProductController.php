<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function __construct()
    {
        //  $this->middleware('abilities:add product', ['only' => 'create','store']);
        //  $this->middleware('abilities:edit product', ['only' => 'edit','update']);
        //  $this->middleware('abilities:delete product', ['only' => 'destroy']);
        //  $this->middleware('abilities:view-product', ['index' => 'show']);

    }
    public function index()
    {
        $products = Product::with('user')->get();
        return $products ;
    }

    public function by_user($id)
    {
        $products = Product::where('user_id', $id)->get();
        return $products;
    }
}
