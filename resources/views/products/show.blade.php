@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Product Details</h2>
                </div>
            </div>
        </div>
        <form action="{{ route('product.update', $product->id) }}" id="edit-product-form" method="post"
            enctype="multipart/form-data" disabled>
            @csrf
            @method('PATCH')
            <input type="hidden" name="id" value="{{ $product->id }}">
            <div class="mb-3">
                <label for="name">Prodcut Name</label>
                <input type="text" name="name" class="form-control" id="name" value="{{ $product->name }}" disabled readonly>
            </div>

            <div class="mb-3 row">
                <div class="col">
                    <img src="{{ Storage::disk('public')->url('images/products/' . $product->image) }}" alt=""
                        class="img-fluid">
                </div>

            </div>
            <div class="mb-3">
                <label for="size" class="form-label">Size</label>
                <select class="form-select" name="size" id="size" readonly disabled>
                    @foreach ($sizes as $size)
                        <option value="{{ $size->name }}" {{ $product->size == $size->name ? 'selected' : '' }}>
                            {{ $size->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="price">Price</label>
                <input type="number" name="price" class="form-control" id="price" value="{{ $product->price }}" disabled readonly>
            </div>
        </form>
    </div>
@endsection


@push('scripts')
@endpush
