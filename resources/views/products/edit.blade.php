@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Edit Product</h2>
                </div>
            </div>
        </div>
        <form action="{{ route('product.update', $product->id) }}" id="edit-product-form" method="post"
            enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <input type="hidden" name="id" value="{{ $product->id }}">
            <div class="mb-3">
                <label for="name">Prodcut Name</label>
                <input type="text" name="name" class="form-control" id="name" value="{{ $product->name }}">
            </div>

            <div class="mb-3 row">
                <div class="col">
                    <label for="image" class="form-label">Product Image</label>
                    <input class="form-control" type="file" id="image" name="image" accept="image/*">
                </div>
                <div class="col">
                    <img src="{{ Storage::disk('public')->url('images/products/' . $product->image) }}" alt=""
                        class="img-fluid">
                </div>

            </div>
            <div class="mb-3 row align-items-end">

                <div class="col">
                    <label for="size" class="form-label">Size</label>
                    <select class="form-select" name="size" id="size">
                        @foreach ($sizes as $size)
                            <option value="{{ $size->name }}" {{ $product->size == $size->name ? 'selected' : '' }}>
                                {{ $size->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Add Size
                    </button>
                </div>
            </div>
            <div class="mb-3">
                <label for="price">Price</label>
                <input type="number" name="price" class="form-control" id="price" value="{{ $product->price }}">
            </div>
            <button type="submit" class="btn btn-success">Edit </button>
        </form>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Add Size</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('product.add_size') }}" id="add-size-form" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="name">Size</label>
                            <input type="text" name="name" class="form-control" id="name">
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
@endpush
