@extends('layouts.app')

@push('styles')
    <style>
        th,
        td {
            text-align: center !important;
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="pull-left">
                    <h2>Products</h2>
                </div>
                @can('add product')
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('product.create') }}">Create Product</a>
                    </div>
                @endcan
            </div>
        </div>
        <hr>
        <table class="table table-striped default-datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Size</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $key => $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->image }}</td>
                        <td>{{ $product->size }}</td>
                        <td>{{ $product->price }}</td>
                        <td class="d-flex justify-content-center align-items-center">
                            @can('view product')
                                <a class="btn btn-transparent" href="{{ route('product.show', $product->id) }}"><i
                                        class="fa fa-eye text-warning"></i></a>
                            @endcan
                            @can('edit product')
                                <a class="btn btn-transparent" href="{{ route('product.edit', $product->id) }}"><i
                                        class="fa fa-edit text-success"></i></a>
                            @endcan
                            @can('delete product')
                                <form action="{{ route('product.destroy', $product->id) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-transparent btn-prompt"><i
                                            class="fa fa-trash text-danger"></i></button>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
