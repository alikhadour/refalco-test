@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="list-group">
            @foreach ($users as $user)
                <a href="#" class="list-group-item list-group-item-action d-flex justify-content-around">
                    <span>{{ $user->name }}</span>
                    <span>{{ $user->email }}</span>
                    <span>Roles :{{ $user->roles->pluck('name') }}</span>
                    <span>Permissions :{{ $user->permissions->pluck('name') }}</span>
                </a>
            @endforeach
        </div>
        <hr>
        <div>
            <form action="{{ route('user.store') }}" id="create-user-form" method="post" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" name="name" id="name" required>
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email address</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="name@refalco.com"
                        required>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" name="password" id="password" required>
                </div>
                <div class="mb-3">
                    <label for="password_confirmation" class="form-label">Confirm Password</label>
                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" required>
                </div>
                <select class="form-select" name="permissions[]" multiple>
                    @foreach ($permissions as $permission)
                        <option value="{{ $permission->name }}">{{ $permission->name }}</option>
                    @endforeach
                </select>
                <div class="mt-2">
                    <button type="submit" class="btn btn-primary">Create User</button>
                </div>
            </form>
        </div>

    </div>
@endsection
