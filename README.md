
# Refalco Test

  

First run `composer install` and `npm install & npm run dev` to install the packages

then migrate the database with and seed the database with the default permissions and one super-admin user `php artisan migrate --seed`

link storage 'for products images upload' with `php artisan storage:link`

  

the user created credentials is :

|username|password |

|--|--|

| super@refalco.com | 12345678 |

  

you should provided with 2 tabs on the navbar for user management and product management.

(depending on your permissions only authorized users can do actions -add product, view product etc....-) create new user from user management section and see for yourself.

  

---

  

### Product API

  

I've provided a postman collection **Refalco.postman_collection.json** contains 4 endpoints

http://127.0.0.1:8000/api/auth/login

using the previous credentials should return an access token to use with calling the other two endpoints.

  

http://127.0.0.1:8000/api/product

returns all the product with the user added this product

  

http://127.0.0.1:8000/api/product/{id}

returns all the products associated with user

  

http://127.0.0.1:8000/api/user

returns the current logged user with attached permissions
