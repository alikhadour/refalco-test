<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();
        $customer_role = Role::create(['name' => 'customer']);

        Permission::create(['name' => 'add product']);
        Permission::create(['name' => 'edit product']);
        Permission::create(['name' => 'view product'])->assignRole($customer_role);
        Permission::create(['name' => 'delete product']);

        Permission::create(['name' => 'add user']);
        Permission::create(['name' => 'edit user']);
        Permission::create(['name' => 'view user']);
        Permission::create(['name' => 'delete user']);


        $super_user =   User::create([
            'name' => 'super',
            'email' => 'super@refalco.com',
            'password' => Hash::make('12345678'),
        ]);

        $role = Role::create(['name' => 'super-admin']);
        $role->syncPermissions(Permission::all());
        $super_user->assignRole('super-admin');



        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
